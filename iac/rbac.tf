locals {
  namespace = "default"
  role = {
    name = "view-limiter"
  }
  sa = {
    name = "reto-devops"
  }
}

resource "kubernetes_role" "view_limiter" {
  metadata {
    name = local.role.name
    namespace = local.namespace
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list"]
  }
}

resource "kubernetes_service_account" "reto_devops" {
  metadata {
    name = local.sa.name
    namespace = local.namespace
  }
}

resource "kubernetes_role_binding" "view_limiter" {
  metadata {
    name = local.role.name
    namespace = local.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role"
    name = local.role.name
  }

  subject {
    kind = "ServiceAccount"
    name = local.sa.name
    namespace = local.namespace
  }

  depends_on = [
    kubernetes_role.view_limiter,
    kubernetes_service_account.reto_devops
  ]
}
