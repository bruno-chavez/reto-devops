[![pipeline status](https://gitlab.com/bruno-chavez/reto-devops/badges/master/pipeline.svg)](https://gitlab.com/bruno-chavez/reto-devops/-/commits/master)

# Reto-DevOps CLM

## ADRs
Antes de revisar la solución a cada reto se pide revisar este README
además de la carpeta [ADRs](ADRs).

## Reto 1 Dockerize la aplicación
Para poder probar este reto se recomienda realizar cualquier cambio “dummy” en la carpeta `app` (ejemplo: agregar una línea vacía en cualquier archivo de la carpeta). 

Esto ejecutará [app.yml](pipelines/app.yml) pipeline.

Dentro de este uno de los jobs es construir y subir una imagen Docker al 
registry de Gitlab (la imagen resultante se encuentra [acá](https://gitlab.com/bruno-chavez/reto-devops/container_registry/2147590)):
```yaml
container_image:
  image: docker:20.10.7
  stage: build
  extends: .base_app
  services:
    - docker:20.10.7-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:latest
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG app
    - docker push $IMAGE_TAG
```

Si se desea probar de manera local es necesario entrar a la carpeta `app` y correr el siguiente comando:
```
$ docker build . -t app
```
Y si se quiere ejecutar un contenedor a partir de esta imagen y probarlo:
```
$ docker run -p 3000:3000 app
$ curl localhost:3000
$ curl localhost:3000/public
$ curl localhost:3000/private
```

## Reto 2. Docker Compose
Para poder probar este reto se recomienda realizar cualquier cambio “dummy” en la carpeta `app` (ejemplo: agregar una línea vacía en cualquier archivo de la carpeta).

Esto ejecutará [app.yml](pipelines/app.yml) pipeline.

Dentro de este uno de los jobs es construir y ejecutar contenedores con `docker-compose` y ejecutar comandos curl para probar la funcionalidad:
```yaml
docker_compose:
  image: docker/compose:debian-1.29.1
  stage: test
  extends: .base_app
  services:
    - docker:20.10.7-dind
  needs: []
  script:
    - apt-get update
    - apt-get install -y curl
    - cd app
    - docker-compose build
    - docker-compose up -d
    - curl --fail docker
    - curl --fail docker/public
    - |
      curl --fail --header 'Authorization: Basic dXNlcjE6dGVzdDE=' docker/private
```

Si se desea probar de manera local es necesario entrar a la carpeta `app` y correr el siguiente comando:
```
$ docker-compose build
$ docker-compose up -d
$ curl --fail localhost
$ curl --fail localhost/public
$ curl --fail --header 'Authorization: Basic dXNlcjE6dGVzdDE=' localhost/private
```

## Reto 3. Probar la aplicación en cualquier sistema CI/CD
Para este reto en específico se utilizaron los jobs declarados [app.yml](pipelines/app.yml)

Podemos detallar el pipeline en las siguientes etapas con sus respectivos jobs:
### scan
#### dockerfile
Encargado de escanear el Dockerfile de la aplicación para análisis estático con [hadolint](https://github.com/hadolint/hadolint):
```yaml
dockerfile:
  image: hadolint/hadolint:v2.6.0-alpine
  stage: scan
  extends: .base_app
  script: hadolint app/Dockerfile
```

### test
#### jest
Encargado de ejecutar pruebas unitarias con Jest
```yaml
jest:
  image: node:12.22.3-slim
  stage: test
  extends: .base_app
  needs: []
  script:
    - cd app
    - npm ci
    - npx jest --coverage
```
#### docker_compose
Encargado de construir y levantar contenedores Docker con `docker-compose` para probar la integracion entre el proxy reverso y la API REST.
```yaml
docker_compose:
  image: docker/compose:debian-1.29.1
  stage: test
  extends: .base_app
  services:
    - docker:20.10.7-dind
  needs: []
  script:
    - apt-get update
    - apt-get install -y curl
    - cd app
    - docker-compose build
    - docker-compose up -d
    - curl --fail docker
    - curl --fail docker/public
    - |
      curl --fail --header 'Authorization: Basic dXNlcjE6dGVzdDE=' docker/private
```
### build
#### container_image
Encargado de construir y subir la imagen Docker de la API REST al registry de Gitlab.
```yaml
container_image:
  image: docker:20.10.7
  stage: build
  extends: .base_app
  services:
    - docker:20.10.7-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:latest
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG app
    - docker push $IMAGE_TAG
```
### deploy
#### kubernetes
Encargado de desplegar los manifestos de Kubernetes
```yaml
kubernetes:
  image:
    name: bitnami/kubectl:1.20.9
    entrypoint: [""]
  stage: deploy
  allow_failure: true
  script:
    - echo "this job will fail since we dont have a kubernetes cluster to deploy to"
    - kubectl apply --dry-run=client -f k8s/deployment.yaml
    - kubectl apply --dry-run=client -f k8s/service.yaml
    - kubectl apply --dry-run=client -f k8s/hpa.yaml
    - kubectl apply -f k8s/deployment.yaml
    - kubectl apply -f k8s/service.yaml
    - kubectl apply -f k8s/hpa.yaml
  rules:
    - changes:
        - app/**/*
        - k8s/*
```

## Reto 4. Deploy en kubernetes
Se pueden encontrar los manifestos de Kubernetes [acá](k8s).

Para poder probar este reto se recomienda realizar cualquier cambio “dummy” en la carpeta `app` (ejemplo: agregar una línea vacía en cualquier archivo de la carpeta).

Esto ejecutará [app.yml](pipelines/app.yml) pipeline.

Si se desea probar de manera local primero es necesario construir 
la imagen de la aplicación siguiendo las indicaciones del [reto #1](#Reto 1 Dockerize la aplicación).

Luego edite el archivo [deployment.yaml](k8s/deployment.yaml) con `image: app`:
```yaml
...
spec:
  containers:
    - name: reto-devops
      image: "app"
      imagePullPolicy: IfNotPresent
...
```

Y por último desplegar los manifestos de Kubernetes, entrando a la carpeta `k8s` y ejecutando:
```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl apply -f k8s/service.yaml
$ kubectl apply -f k8s/hpa.yaml
```

## Reto 5. Construir Chart en helm y manejar trafico http(s)
Se puede encontrar el chart de nginx [acá](charts/nginx).

Para poder probar este reto se recomienda realizar cualquier cambio “dummy” en la carpeta `charts/nginx` (ejemplo: agregar una línea vacía en cualquier archivo de la carpeta).

Esto ejecutará [helm-chart.yml](pipelines/helm-chart.yml) pipeline.

Si se desea probar de manera local es necesario entrar a la carpeta `charts/nginx` y correr los siguientes comandos:
```
$ helm package nginx
$ helm install nginx ./nginx-1.0.0.tgz
```


## Reto 6. Terraform
Se puede encontrar los archivos HCL [acá](iac).

Para poder probar este reto se recomienda realizar cualquier cambio “dummy” en la carpeta `iac` (ejemplo: agregar una línea vacía en cualquier archivo de la carpeta).

Esto ejecutará [iac.yml](pipelines/iac.yml) pipeline.

Si se desea probar de manera local es necesario entrar a la carpeta `iac` y posiblemente configurar el [provider de kubernetes](iac/main.tf):
```hcl
provider "kubernetes" {}
```

Y correr los siguientes comando:
```
$ terraform init
$ terraform apply
```

## Reto 7. Automatiza el despliegue de los retos realizados
Leer [ADR](ADRs/replacing-makefile.md) relacionado con este reto.

# Notas
* Este README es el único documento en español para facilitar la revision del reto, sin embargo
el resto de documentos, scripts, código, etc utilizados estará en ingles por decision 