# Restructuring the repository to facilitate different executions of the CI/CD pipeline

## Context
With the original repository structure it was very hard to not trigger more than one context at a time (example: triggering the kubernetes app deploy when changing a Terraform file).

## Decision
I decided to split the repo in five main folders:

* `app`: Contains everything related to challenge #1 and #2.
* `pipelines`: Contains everything related to challenge #3.
* `k8s`: Contains everything related to challenge #4.
* `charts`: Contains everything related to challenge #5.
* `iac`: Contains everything related to challenge #6.
