# CI/CD jobs that belong to the 'deploy' stage fail on purpose to shorten the challenge

## Context
Its 3am in the morning, and I don't want to spin up a remote Kubernetes cluster (that's going to cost me money!) and connect it to this pipeline.


## Decision
I decided to mark all "deploy" jobs with `allow_failure:true` and put a disclaimer on the execution. 

If you need to test the challenges locally, I'll leave some overly simplistic commands to test them out (I don't guarantee that they are going to work out due to the amount of variables out of my control. You can read more about this [here](replacing-makefile.md)).
