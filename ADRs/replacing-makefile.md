# Replacing Makefile with a CI/CD pipeline for challenge #7

## Context

For challenge #7 I was tasked with creating a `Makefile` in order to deploy all previous challenges. 

In general, I found those to be brittle and prone to fail since everyone has different configurations, environments, operative systems, tool versions, etc and is pretty hard to account for all of that to have a truly successful `Makefile`.

## Decision

I decided to create a CI/CD pipeline that builds and deploys the challenges #1 - #6 (skipping #3 since that one specifically asks for a CI/CD pipeline).

Note that every deploy job is going to fail due to not having properly configured a remote kubernetes cluster. You can read more about this decision [here](deploy-jobs-fail-on-purpose.md).

The pipeline has its entrypoint in [.gitlab-ci.yml](../.gitlab-ci.yml) which loads all the pipeline definitions found in the [pipelines folder](../pipelines).

This design makes it possible to have three different independent context (building and deploying a backend and, a Helm Chart and some Terraform HCL files).

In order to trigger each context its only needed to upload changes to each corresponding folder ([app](../app), [charts/nginx](../charts/nginx) and [iac](../iac)).

The use of [parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html) for this use case was ideal but its 4am and I want to sleep, so the refactor is going to be left out of the challenge (Should have made the pipeline that way since the beginning!).